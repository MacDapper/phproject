<?php

session_start();
$host = "localhost";
$user = "root";
$password = "mysqlroot";
$db = "phpForum";

include_once "controllers/UserController.php";
include_once "controllers/ForumController.php";
include_once 'controllers/CategoryController.php';
include_once 'controllers/SubForumController.php';
//include_once "controllers/SubForumController.php";
//include_once "controllers/TopicController.php";
include_once "controllers/ConnectionController.php";
include_once "controllers/TopicController.php";
include_once "controllers/ReplyController.php";




$route = explode("/", @$_SERVER[REQUEST_URI]);
$requestURI = $correction = explode('/', $_SERVER['REQUEST_URI']);
$scriptName = explode('/', $_SERVER['SCRIPT_NAME']);

//chk static files
for ($i = 0; $i < sizeof($scriptName); $i++) {
    if ($requestURI[$i] == $scriptName[$i]) {
        $root_path[] = $requestURI[$i];
        unset($requestURI[$i]);
    }
}

$command = array_values($requestURI);
$lastElement = $command[count($command) - 1];
$pattern = "/.*\..*/";
$file = preg_match($pattern, $lastElement);
$path = implode("/", array_slice(explode("/", @$_SERVER[REQUEST_URI]), 0, -2));
if ($file) {
    $correctionIndex = count($root_path);
    $correction[$correctionIndex] = "view";
    $corrected_path = implode("/", $correction);
    //include_once @$_SERVER[DOCUMENT_ROOT].$corrected_path;
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . $corrected_path))
        header('Location:' . $corrected_path);
    else
        include_once "view/admin-404.php";
}
$connection = new ConnectionController($host, $user, $password, $db);
$user = new UserController($connection, "user");
$forum = new ForumController($connection, "mainForum", "Open Source :)");
$subForum = new subForumController($connection, "forum");
$category = new CategoryController($connection, "category");
$topic = new TopicController($connection, "topic");
$reply = new ReplyController($connection, "reply");
if (isset($_SESSION['loginUser']))
    switch ($command[0]) {
        //user access
        case "user":
            switch ($command[1]) {
                //already logged-in
//                case "login":
//                    $user->isValidAction($_SESSION['email'], $_SESSION['password']);
//                    break;
                case (preg_match('/view\?id=[0-9]+/', $command[1]) ? true : false):
                    $user->profileAction($_GET['id']);
                    break;
                case "list":
                    $user->listUserAction();
                    break;

                case (preg_match('/update\?id=[0-9]+/', $command[1]) ? true : false):
                    switch ($_SERVER['REQUEST_METHOD']) {
                        case 'GET':
                            $user->registerAction($_GET['id']);
                            break;
                        case 'POST':
                            $user->userUpdateAction();
                            break;
                    }
                    break;
                //add only if logged as admin
                case "add":
                    switch ($_SERVER['REQUEST_METHOD']) {
                        case 'POST':
                            $user->saveUserAction();
                            break;

                        case 'GET':
                            $user->registerAction(array());
                            break;
                    }
                    break;
                case "logout":
                    $user->logoutAction();
                    break;
                case (preg_match('/delete\?id=[0-9]+/', $command[1]) ? true : false):
                    $user->deleteUser($_GET['id']);
                    break;
                default:
                    include_once "view/admin-404.php";
                    break;
            }
            break;

        //main forum access
        case "forum":
            switch ($command[1]) {
                //using buttons
                case "togglestatus":
                    $forum->toggleStatus();
                    break;

//                //name & status using form
//                case "update":
//                     switch ($_SERVER['REQUEST_METHOD']) {
//                        case 'GET':
//                            $forum->update();
//                            break;
//                        case 'POST':
//                            $forum->saveUpdate();
//                            break;
//                    }
//                    break;
                //list categories with its subforums
                case "main":
                    $forum->main();
                    break;
            }
            break;

        case "category":
            switch ($command[1]) {
                case "add":
                    switch ($_SERVER['REQUEST_METHOD']) {
                        case 'GET':
                            $category->CategoryForm();
                            break;
                        case 'POST':
                            $category->SaveCategory();
                            break;
                    }
                    break;
                case (preg_match('/delete\?id=[0-9]+/', $command[1]) ? true : false):
                    $category->deleteCategory();
                    break;
                //name & status
                case (preg_match('/update\?id=[0-9]+/', $command[1]) ? true : false):
                    switch ($_SERVER['REQUEST_METHOD']) {
                        case 'GET':
                            $category->CategoryForm();
                            break;
                        case 'POST':
                            $category->SaveCategory();
                            break;
                    }
                    break;
                //list category's subforums
                //no view yet
//                case "list":
//                    
//                    break;
            }
            break;

        case "subForum":
            switch ($command[1]) {
                case (preg_match('/add\?categoryId=[0-9]+/', $command[1]) ? true : false):
                    switch ($_SERVER['REQUEST_METHOD']) {
                        case 'GET':
                            $subForum->SubForumForm();
                            break;
                        case 'POST':
                            $subForum->SaveSubForum();
                            break;
                    }
                    break;
                case (preg_match('/delete\?id=[0-9]+/', $command[1]) ? true : false):
                    $subForum->deleteSubForum();
                    break;
                //name & status
                case (preg_match('/update\?id=[0-9]+/', $command[1]) ? true : false):
                    switch ($_SERVER['REQUEST_METHOD']) {
                        case 'GET':
                            $subForum->SubForumForm();
                            break;
                        case 'POST':
                            $subForum->SaveSubForum();
                            break;
                    }
                    break;
                //list subforum's topics
                case "(preg_match('/view\?id=[0-9]+/', $command[1]) ? true : false)":
                    break;
            }
            break;
        case "topic":
            switch ($command[1]) {
                case (preg_match('/add\?forumId=[0-9]+/', $command[1]) ? true : false):
                    $topic->addTopicAction();
                    break;
                case "add":
                    $topic->addTopicAction();
                    break;
                case (preg_match('/list\?forumId=[0-9]+/', $command[1]) ? true : false):
                    $topic->listTopicAction();
                    break;
                //admin
                case (preg_match('/delete\?id=[0-9]+/', $command[1]) ? true : false):
                    $topic->deleteTopicAction($_GET['id']);
                    break;
                //
                case (preg_match('/update\?id=[0-9]+/', $command[1]) ? true : false):
                    $topic->updateTopicAction();
                    break;
                //show the topic with it's replies
                case (preg_match('/view\?id=[0-9]+/', $command[1]) ? true : false):
                    $topic->viewTopicAction($_GET['id'], '');
                    break;
            }
            break;
        case "reply":
            switch ($command[1]) {
                case "add":
                    $reply->addReplyAction();
                    break;
                //admin
                case (preg_match('/delete\?id=[0-9]+/', $command[1]) ? true : false):
                    $reply->deleteReplyAction($_GET['id']);
                    break;
                //
                case "update":
                    $reply->updateReplyAction();
                    break;
            }
            break;
        default:
            include_once "view/admin-404.php";
            break;
    } elseif (!isset($_SESSION['loginUser']) && $forum->getStatus() == "Working") {
    switch ($command[0]) {
        case "user":
            switch ($command[1]) {
                case "register":
                    switch ($_SERVER['REQUEST_METHOD']) {
                        case 'GET':
                            $user->registerAction(array());
                            break;
                        case 'POST':
                            $user->saveUserAction();
                            break;
                    }

                    break;
                case "login":
                    switch ($_SERVER['REQUEST_METHOD']) {
                        case 'GET':
                            $user->loginAction(array(""));
                            break;
                        case 'POST':
                            $user->isValidAction($_POST['email'], $_POST['password']);
                            break;
                    }
                    break;
            }
            break;
        case "topic":
            switch ($command[1]) {
                case (preg_match('/list\?forumId=[0-9]+/', $command[1]) ? true : false):
                    $topic->listTopicAction();
                    break;

                //show the topic with it's replies
                case (preg_match('/view\?id=[0-9]+/', $command[1]) ? true : false):
                    $topic->viewTopicAction($_GET['id'], '');
                    break;
            }
            break;

//        needn't as it is the default
//        case "forum":
//            switch ($command[1])
//            {
//            case "main":   
//                $forum->main();
//                break;
//            }
//        break;
        default :
            $forum->main();
            break;
    }
} elseif (!isset($_SESSION['loginUser'])) {
    switch ($command[0]) {
        case "user":
            switch ($command[1]) {
                case "login":
                    switch ($_SERVER['REQUEST_METHOD']) {
                        case 'GET':
                            $user->loginAction(array(""));
                            break;
                        case 'POST':
                            $user->isValidAction($_POST['email'], $_POST['password']);
                            break;
                    }
                    break;
            }
        default :
            echo "Maintanence. <a href='" . $path . "/user/login'>Login</a>";
    }
} else {
    
}
?>
