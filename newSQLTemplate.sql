
CREATE TABLE `user` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `role` enum('Admin','User') NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `signature` varchar(200) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `country` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
);
-----------------------------------------------------------------------
CREATE TABLE `mainForum` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` enum('Working','Not Working') NOT NULL,
  PRIMARY KEY (`id`)
);
-----------------------------------------------------------------------
CREATE TABLE `category` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` enum('Accept','Not Accept') NOT NULL,
  `forumId` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forumId` (`forumId`),
  CONSTRAINT `category_ibfk_1` FOREIGN KEY (`forumId`) REFERENCES `mainForum` (`id`)ON DELETE CASCADE
);
-----------------------------------------------------------------------
CREATE TABLE `forum` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` enum('Accept','Not Accept') NOT NULL,
  `categoryId` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categoryId` (`categoryId`),
  CONSTRAINT `forum_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `category` (`id`)ON DELETE CASCADE
);
-----------------------------------------------------------------------
CREATE TABLE `topic` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `body` varchar(300) NOT NULL,
  `status` enum('Accept','Not Accept') NOT NULL,
  `stickiness` enum('Sticky','Not Sticky') NOT NULL,
  `forumId` int(3) NOT NULL,
  `userId` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forumId` (`forumId`),
  KEY `userId` (`userId`),
  CONSTRAINT `topic_ibfk_1` FOREIGN KEY (`forumId`) REFERENCES `forum` (`id`) ON DELETE CASCADE,
  CONSTRAINT `topic_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE
);
------------------------------------------------------------------------
  CREATE TABLE `reply` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `body` varchar(300) NOT NULL,
  `topicId` int(3) NOT NULL,
  `userId` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `topicId` (`topicId`),
  KEY `userId` (`userId`),
  CONSTRAINT `reply_ibfk_1` FOREIGN KEY (`topicId`) REFERENCES `topic` (`id`) ON DELETE CASCADE,
  CONSTRAINT `reply_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE
  );