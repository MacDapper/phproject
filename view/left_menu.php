<?php
$path = implode("/", array_slice(explode("/", @$_SERVER[REQUEST_URI]), 0, -2));
?>
<div class="responsive-admin-menu">
    <div class="responsive-menu">
        <div class="menuicon"><i class="fa fa-angle-down"></i></div>
    </div>
    <ul id="menu">
        <li><a title="Dashboard" data-id="dash-sub"><i class="entypo-comment"></i><span> Hello, <?php echo @$_SESSION['loginUser']['name'] ?></span></a>
        </li>
        <li><a href="<?php echo @$path . '/forum/main'?>" title="Dashboard" data-id="dash-sub"><i class="entypo-comment"></i><span> Home</span></a>
        </li>
        <li><a href="#" title="Inbox"><i class="entypo-inbox"></i><span> Inbox <span class="badge">32</span></span></a></li>
        </li>
        <li><a href="<?php echo @$path . "/user/view?id=" . @$_SESSION['loginUser']['id'] ?>" title="Profile"><i class="entypo-user"></i><span> Profile </span></a></li>
        <li><a href="<?php echo @$path . "/user/list"?>" title="Items List"><i class="entypo-list"></i><span> <span style="display: <?php echo @View::display('')?>">Manage</span> Users</span></a></li>
        </li>
    </ul>
</div>



