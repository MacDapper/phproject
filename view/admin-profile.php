<?php 
$user=$data;
?>
﻿<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="keywords" content="admin template, admin dashboard, inbox templte, calendar template, form validation">
    <meta name="author" content="DazeinCreative">
    <meta name="description" content="ORB - Powerfull and Massive Admin Dashboard Template with tonns of useful features">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>User  | Profile</title>
    <link href="css/styles.css" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <script type="text/javascript" src="js/vendors/modernizr/modernizr.custom.js"></script>
</head>

<body>

    <!--Smooth Scroll-->
    <div class="smooth-overflow">
        <!--Navigation-->
        <?php include_once 'nav_menu.php'; ?>
        <!--/Navigation-->     
        <!--MainWrapper-->
        <div class="main-wrap"> 

            <!--OffCanvas Menu -->
            <aside class="user-menu"> 

                <!-- Tabs -->
                <div class="tabs-offcanvas">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#userbar-one" data-toggle="tab">Main</a></li>
                        <li><a href="#userbar-two" data-toggle="tab">Users</a></li>
                        <li><a href="#userbar-three" data-toggle="tab">ToDo</a></li>
                    </ul>
                    <div class="tab-content"> 

                        <!--User Primary Panel-->
                        <div class="tab-pane active" id="userbar-one">
                            <div class="main-info">
                                <div class="user-img"><img src="http://placehold.it/150x150" alt="User Picture" /></div>
                                <h1>Anton Durant <small>Administrator</small></h1>
                            </div>
                            <div class="list-group"> <a href="#" class="list-group-item"><i class="fa fa-user"></i>Profile</a> <a href="#" class="list-group-item"><i class="fa fa-cog"></i>Settings</a> <a href="#" class="list-group-item"><i class="fa fa-flask"></i>Projects<span class="badge">2</span></a>
                                <div class="empthy"></div>
                                <a href="#" class="list-group-item"><i class="fa fa-refresh"></i>Updates<span class="badge">5</span></a> <a href="#" class="list-group-item"><i class="fa fa-comment"></i>Messages<span class="badge">12</span></a> <a href="#" class="list-group-item"><i class="fa fa-comments"></i> Comments<span class="badge">45</span></a>
                                <div class="empthy"></div>
                                <a href="#" data-toggle="modal" class="list-group-item lockme"><i class="fa fa-lock"></i> Lock</a> <a data-toggle="modal" href="#" class="list-group-item goaway"><i class="fa fa-power-off"></i> Sign Out</a> </div>
                        </div>

                        <!--User Chat Panel-->
                        <div class="tab-pane" id="userbar-two">
                            <div class="chat-users-menu"> 
                                <!--Adding Some Scroll-->
                                <div class="nano">
                                    <div class="nano-content">
                                        <div class="buttons">
                                            <div class="btn-group btn-group-xs">
                                                <button type="button" class="btn btn-default">Friends</button>
                                                <button type="button" class="btn btn-default">Work</button>
                                                <button type="button" class="btn btn-default">Girls</button>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><a href="#"><span class="chat-name">Gluck Dorris</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span><span class="badge">5</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-default">Offline</span></a></li>
                                            <li><a href="#"><span class="chat-name">Mr. Joker</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Chewbacca</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">The Piggy</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-default">Offline</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--User Tasks Panel-->
                        <div class="tab-pane" id="userbar-three">
                            <div class="nano"> 
                                <!--Adding Some Scroll-->
                                <div class="nano-content">
                                    <div class="small-todos">
                                        <div class="input-group input-group-sm">
                                            <input id="new-todo" placeholder="Add ToDo" type="text" class="form-control">
                                            <span class="input-group-btn">
                                                <button id="add-todo" class="btn btn-default" type="button"><i class="fa fa-plus-circle"></i></button>
                                            </span> </div>
                                        <section id="task-list">
                                            <ul id="todo-list">
                                            </ul>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /tabs --> 

            </aside>
            <!-- /Offcanvas user menu--> 

            <!--Main Menu-->
                  <?php $loginUser=@$data['loginUser']; include_once 'left_menu.php'; ?>
            <!--/MainMenu-->

            <div class="content-wrapper"> 
                <!--Content Wrapper-->
                <!--Breadcrumb-->
                <div class="breadcrumb clearfix">
                    <ul>
                        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active">Profile</li>
                    </ul>
                </div>
                <!--/Breadcrumb-->

                <div class="page-header">
                    <h1>Profile<small></small><a style="display: <?php echo @View::display(@$user['id'])?>" style="  font-size: 22px;" href='<?php echo @$path . "/user/update?id=" . @$user['id'] ?>'>edit</a></h1>
                </div>

                <!-- Widget Row Start grid -->
                <div class="row" id="powerwidgets">
                    <div class="col-md-12 bootstrap-grid"> 

                        <!-- New widget -->

                        <div class="powerwidget cold-grey" id="profile" data-widget-editbutton="false">
                            
                            <div class="inner-spacer"> 

                                <!--Profile-->
                                <div class="user-profile">
                                    <div class="main-info">
                                        <div class="user-img"><img src="http://placehold.it/150x150" alt="User Picture" /></div>
                                        <h1><?php
                            echo @$user['name']
                            ?></h1>
                                        Followers: 451 | Friends: 45 | Items: 22 </div>
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="item item1 active"> </div>
                                        </div>
                                    <div class="user-profile-info">
                                        <div class="tabs-white">
                                            <ul id="myTab" class="nav nav-tabs nav-justified">
                                                <li class="active"><a href="#home" data-toggle="tab">About</a></li>
                                                <li><a href="#followers" data-toggle="tab">Followers</a></li>
                                                <li><a href="#activity" data-toggle="tab">Activity</a></li>
                                                <li><a href="#blog" data-toggle="tab">Blogs</a></li>
                                                <li><a href="#chat" data-toggle="tab">Chat</a></li>
                                            </ul>
                                            <div id="myTabContent" class="tab-content">
                                                <div class="tab-pane in active" id="home">
                                                    <div class="profile-header">Signature</div>
                                                    <p><?php echo @$user['signature']?></p>
                                                    <table class="table">
                                                        <tr>
                                                            <td><strong>Name:</strong></td>
                                                            <td><?php echo @$user['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Email:</strong></td>
                                                            <td><?php echo @$user['email']?></td>
                                                            <td><strong>Role:</strong></td>
                                                            <td><?php echo @$user['role']?></td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Status:</strong></td>
                                                            <td><?php echo @$user['status']?></td>
                                                            <td><strong>Username:</strong></td>
                                                            <td><?php echo @$user['username']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Gender:</strong></td>
                                                            <td><?php echo @$user['gender']?></td>
                                                            <td><strong>Country:</strong></td>
                                                            <td><?php echo @$user['country']?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <!--Chat Tab-->
                                            </div>

                                            <!--/Chat Tab-->

                                            <div class="social-buttons">
                                                <ul class="social">
                                                    <li><a href="http://facebook.com/"><i class="entypo-facebook-circled"></i></a></li>
                                                    <li><a href="http://linkedin.com/"><i class="entypo-linkedin-circled"></i></a></li>
                                                    <li><a href="http://google.com/"><i class="entypo-gplus-circled"></i></a></li>
                                                    <li><a href="http://twitter.com/"><i class="entypo-twitter-circled"></i></a></li>
                                                    <li><a href="http://pinterest.com/"><i class="entypo-pinterest-circled"></i></a></li>
                                                    <li><a href="http://tumblr.com/"><i class="entypo-tumblr-circled"></i></a></li>
                                                    <li><a href="http://stumbleupon.com/"><i class="entypo-stumbleupon-circled"></i></a></li>
                                                    <li><a href="http://dribble.com/"><i class="entypo-dribbble-circled"></i></a></li>
                                                    <li><a href="http://vimeo.com/"><i class="entypo-vimeo-circled"></i></a></li>
                                                    <li><a href="http://mixi.com/"><i class="entypo-mixi"></i></a></li>
                                                    <li><a href="http://lastfm.com/"><i class="entypo-lastfm-circled"></i></a></li>
                                                    <li><a href="http://instagram.com/"><i class="entypo-instagram"></i></a></li>
                                                    <li><a href="http://vk.com/"><i class="entypo-vkontakte"></i></a></li>
                                                    <li><a href="http://flickr.com/"><i class="entypo-flickr-circled"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--/Profile--> 
                        </div>
                    </div>
                    <!-- End .powerwidget --> 

                </div>
                <!-- /Inner Row Col-md-12 --> 
            </div>
            <!-- /Widgets Row End Grid--> 
        </div>
        <!-- / Content Wrapper --> 
    </div>
    <!--/MainWrapper--> 
</div>
<!--/Smooth Scroll--> 


<!-- scroll top -->
<div class="scroll-top-wrapper hidden-xs">
    <i class="fa fa-angle-up"></i>
</div>
<!-- /scroll top -->



<!--Modals-->

<!--Power Widgets Modal-->
<div class="modal" id="delete-widget">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="fa fa-lock"></i> </div>
            <div class="modal-body text-center">
                <p>Are you sure to delete this widget?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="trigger-deletewidget-reset">Cancel</button>
                <button type="button" class="btn btn-primary" id="trigger-deletewidget">Delete</button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<!--Sign Out Dialog Modal-->
<div class="modal" id="signout">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="fa fa-lock"></i> </div>
            <div class="modal-body text-center">Are You Sure Want To Sign Out?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="yesigo">Ok</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<!--Lock Screen Dialog Modal-->
<div class="modal" id="lockscreen">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="fa fa-lock"></i> </div>
            <div class="modal-body text-center">Are You Sure Want To Lock Screen?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="yesilock">Ok</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<!--Scripts--> 
<!--JQuery--> 
<script type="text/javascript" src="js/vendors/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script> 

<!--Fullscreen--> 
<script type="text/javascript" src="js/vendors/fullscreen/screenfull.min.js"></script> 

<!--NanoScroller--> 
<script type="text/javascript" src="js/vendors/nanoscroller/jquery.nanoscroller.min.js"></script> 

<!--Sparkline--> 
<script type="text/javascript" src="js/vendors/sparkline/jquery.sparkline.min.js"></script> 

<!--Horizontal Dropdown--> 
<script type="text/javascript" src="js/vendors/horisontal/cbpHorizontalSlideOutMenu.js"></script> 
<script type="text/javascript" src="js/vendors/classie/classie.js"></script> 

<!--PowerWidgets--> 
<script type="text/javascript" src="js/vendors/powerwidgets/powerwidgets.min.js"></script> 

<!--Bootstrap--> 
<script type="text/javascript" src="js/vendors/bootstrap/bootstrap.min.js"></script> 

<!--ToDo--> 
<script type="text/javascript" src="js/vendors/todos/todos.js"></script> 

<!--Main App--> 
<script type="text/javascript" src="js/scripts.js"></script>



<!--/Scripts-->

</body>
</html>