﻿<!DOCTYPE html>
<?php
$path =  implode("/",array_slice(explode("/",@$_SERVER[REQUEST_URI]),0,-2));
?>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="keywords" content="admin template, admin dashboard, inbox templte, calendar template, form validation">
    <meta name="author" content="DazeinCreative">
    <meta name="description" content="ORB - Powerfull and Massive Admin Dashboard Template with tonns of useful features">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ORB | Form Validation and Processing</title>
    <link href="css/styles.css" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <script type="text/javascript" src="js/vendors/modernizr/modernizr.custom.js"></script>
</head>

<body>

    <!--Smooth Scroll-->
    <div class="smooth-overflow">
        <!--Navigation-->
        <?php include_once 'nav_menu.php';?>

        <!--/Navigation--> 

        <!--MainWrapper-->
        <div class="main-wrap"> 

            <!--OffCanvas Menu -->
            <aside class="user-menu"> 

                <!-- Tabs -->
                <div class="tabs-offcanvas">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#userbar-one" data-toggle="tab">Main</a></li>
                        <li><a href="#userbar-two" data-toggle="tab">Users</a></li>
                        <li><a href="#userbar-three" data-toggle="tab">ToDo</a></li>
                    </ul>
                    <div class="tab-content"> 

                        <!--User Primary Panel-->
                        <div class="tab-pane active" id="userbar-one">
                            <div class="main-info">
                                <div class="user-img"><img src="http://placehold.it/150x150" alt="User Picture" /></div>
                                <h1>Anton Durant <small>Administrator</small></h1>
                            </div>
                            <div class="list-group"> <a href="#" class="list-group-item"><i class="fa fa-user"></i>Profile</a> <a href="#" class="list-group-item"><i class="fa fa-cog"></i>Settings</a> <a href="#" class="list-group-item"><i class="fa fa-flask"></i>Projects<span class="badge">2</span></a>
                                <div class="empthy"></div>
                                <a href="#" class="list-group-item"><i class="fa fa-refresh"></i>Updates<span class="badge">5</span></a> <a href="#" class="list-group-item"><i class="fa fa-comment"></i>Messages<span class="badge">12</span></a> <a href="#" class="list-group-item"><i class="fa fa-comments"></i> Comments<span class="badge">45</span></a>
                                <div class="empthy"></div>
                                <a href="#" data-toggle="modal" class="list-group-item lockme"><i class="fa fa-lock"></i> Lock</a> <a data-toggle="modal" href="#" class="list-group-item goaway"><i class="fa fa-power-off"></i> Sign Out</a> </div>
                        </div>

                        <!--User Chat Panel-->
                        <div class="tab-pane" id="userbar-two">
                            <div class="chat-users-menu"> 
                                <!--Adding Some Scroll-->
                                <div class="nano">
                                    <div class="nano-content">
                                        <div class="buttons">
                                            <div class="btn-group btn-group-xs">
                                                <button type="button" class="btn btn-default">Friends</button>
                                                <button type="button" class="btn btn-default">Work</button>
                                                <button type="button" class="btn btn-default">Girls</button>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><a href="#"><span class="chat-name">Gluck Dorris</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span><span class="badge">5</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-default">Offline</span></a></li>
                                            <li><a href="#"><span class="chat-name">Mr. Joker</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Chewbacca</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">The Piggy</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-default">Offline</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--User Tasks Panel-->
                        <div class="tab-pane" id="userbar-three">
                            <div class="nano"> 
                                <!--Adding Some Scroll-->
                                <div class="nano-content">
                                    <div class="small-todos">
                                        <div class="input-group input-group-sm">
                                            <input id="new-todo" placeholder="Add ToDo" type="text" class="form-control">
                                            <span class="input-group-btn">
                                                <button id="add-todo" class="btn btn-default" type="button"><i class="fa fa-plus-circle"></i></button>
                                            </span> </div>
                                        <section id="task-list">
                                            <ul id="todo-list">
                                            </ul>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /tabs --> 

            </aside>
            <!-- /Offcanvas user menu--> 

            <!--Main Menu-->
            <?php include_once 'left_menu.php'; ?>

            <!--/MainMenu-->

            <!--Content Wrapper-->
            <div class="content-wrapper"> 
                <!--Horisontal Dropdown-->
                <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
                    <div class="cbp-hsinner">
                        <ul class="cbp-hsmenu">
                            <li> <a href="#"><span class="icon-bar"></span></a>
                                <ul class="cbp-hssubmenu">
                                    <li><a href="#">
                                            <div class="sparkle-dropdown"><span class="inlinebar">10,8,8,7,8,9,7,8,10,9,7,5</span>
                                                <p class="sparkle-name">project income</p>
                                                <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                                            </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="sparkle-dropdown"><span class="linechart">5,6,7,9,9,5,3,2,9,4,6,7</span>
                                                <p class="sparkle-name">site traffic</p>
                                                <p class="sparkle-amount">122541 <i class="fa fa-chevron-circle-down"></i></p>
                                            </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="sparkle-dropdown"><span class="simpleline">9,6,7,9,3,5,7,2,1,8,6,7</span>
                                                <p class="sparkle-name">Processes</p>
                                                <p class="sparkle-amount">890 <i class="fa fa-plus-circle"></i></p>
                                            </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="sparkle-dropdown"><span class="inlinebar">10,8,8,7,8,9,7,8,10,9,7,5</span>
                                                <p class="sparkle-name">orders</p>
                                                <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                                            </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="sparkle-dropdown"><span class="piechart">1,2,3</span>
                                                <p class="sparkle-name">active/new</p>
                                                <p class="sparkle-amount">500/200 <i class="fa fa-chevron-circle-up"></i></p>
                                            </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="sparkle-dropdown"><span class="stackedbar">3:6,2:8,8:4,5:8,3:6,9:4,8:1,5:7,4:8,9:5,3:5</span>
                                                <p class="sparkle-name">fault/success</p>
                                                <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                                            </div>
                                        </a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>

                <!--Breadcrumb-->
                <div class="breadcrumb clearfix">
                    <ul>
                        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active">Topic Details Update</li>
                    </ul>
                </div>
                <!--/Breadcrumb-->


                <!-- Widget Row Start grid -->
                <div class="row" id="powerwidgets">


                    <!-- /Inner Row Col-md-12 -->


                    <div class="col-md-6 bootstrap-grid sortable-grid ui-sortable"> 

                        <!-- New widget -->

                        <div class="powerwidget pink powerwidget-sortable" id="registration-form-validation-widget" data-widget-editbutton="false" role="widget">
                            <header role="heading"><?php echo @$data['errMsg'] ?>
                                <div class="powerwidget-ctrls" role="menu"> <a href="#" class="button-icon powerwidget-delete-btn"><i class="fa fa-times-circle"></i></a>  <a href="#" class="button-icon powerwidget-fullscreen-btn"><i class="fa fa-arrows-alt "></i></a> <a href="#" class="button-icon powerwidget-toggle-btn"><i class="fa fa-chevron-circle-up "></i></a></div><span class="powerwidget-loader"></span></header>
                            <div class="inner-spacer" role="content">
                                <form action="" method="post" id="registration-form" class="orb-form" novalidate="novalidate">
                                    <header>Topic Details</header>
                                    <fieldset>
                                        <section>
                                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                                Title
                                                <input type="name" name="title" placeholder="title" value="<?php echo @$data['title'] ?>" required>
                                                <b class="tooltip tooltip-bottom-right">Needed </b> </label>
                                        </section>
                                        <section>
                                            <label class="input"> <i class=""></i>
                                                Body
                                                <input style="height: 100px" type="textarea" name="body" value="<?php echo @$data['body'] ?>" required>
                                                <b class="tooltip tooltip-bottom-right">Needed </b> </label>
                                        </section>
                                    </fieldset>
                                    <section>
                                            <label class="select">
                                                current status is <?php echo @$data['status'] ?>
                                                <select name="status" required>
                                                    <option value="Accept">Accept Replies</option>
                                                    <option value="Not Accept">Not Accept Replies</option>
                                                </select>
                                                <i></i> </label>
                                        </section>
                                                                        <section>
                                            <label class="select">
                                                current stickiness is <?php echo @$data['stickiness'] ?>
                                                <select name="stickiness" required>
                                                    <option value="Sticky">Sticky</option>
                                                    <option value="Not Sticky">Not Sticky</option>
                                                </select>
                                                <i></i> </label>
                                        </section>
                                    <fieldset>                                                                                
                                                <input type="hidden" name="forumId" value="<?php echo @$data['forumId'] ?>">
                                                <input type="hidden" name="userId" value="<?php echo @$_SESSION['loginUser']['id'] ?>">
                                                <input type="hidden" name="id" value="<?php echo @$data['id'] ?>">

                                    </fieldset>
                                    <footer>
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    </footer>
                                </form>
                            </div>
                        </div>
                        <!-- /End Widget --> 

                        <!-- New widget -->


                        <!-- /End Widget --> 

                    </div>
                    <!-- /Inner Row Col-md-6 --> 
                </div>
                <!-- /Widgets Row End Grid--> 
            </div>
            <!-- / Content Wrapper --> 
        </div>
        <!--/MainWrapper--> 
    </div>
    <!--/Smooth Scroll--> 


    <!-- scroll top -->
    <div class="scroll-top-wrapper hidden-xs">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- /scroll top -->



    <!--Modals-->

    <!--Power Widgets Modal-->
    <div class="modal" id="delete-widget">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <i class="fa fa-lock"></i> </div>
                <div class="modal-body text-center">
                    <p>Are you sure to delete this widget?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="trigger-deletewidget-reset">Cancel</button>
                    <button type="button" class="btn btn-primary" id="trigger-deletewidget">Delete</button>
                </div>
            </div>
            <!-- /.modal-content --> 
        </div>
        <!-- /.modal-dialog --> 
    </div>
    <!-- /.modal --> 

    <!--Sign Out Dialog Modal-->
    <div class="modal" id="signout">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <i class="fa fa-lock"></i> </div>
                <div class="modal-body text-center">Are You Sure Want To Sign Out?</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="yesigo">Ok</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content --> 
        </div>
        <!-- /.modal-dialog --> 
    </div>
    <!-- /.modal --> 

    <!--Lock Screen Dialog Modal-->
    <div class="modal" id="lockscreen">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <i class="fa fa-lock"></i> </div>
                <div class="modal-body text-center">Are You Sure Want To Lock Screen?</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="yesilock">Ok</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content --> 
        </div>
        <!-- /.modal-dialog --> 
    </div>
    <!-- /.modal --> 

    <!--Scripts--> 
    <!--JQuery--> 
    <script type="text/javascript" src="js/vendors/jquery/jquery.min.js"></script> 
    <script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script> 

    <!--Fullscreen--> 
    <script type="text/javascript" src="js/vendors/fullscreen/screenfull.min.js"></script> 

    <!--Forms--> 
    <script type="text/javascript" src="js/vendors/forms/jquery.form.min.js"></script> 
    <script type="text/javascript" src="js/vendors/forms/jquery.validate.min.js"></script> 
    <script type="text/javascript" src="js/vendors/forms/jquery.maskedinput.min.js"></script> 
    <script type="text/javascript" src="js/vendors/jquery-steps/jquery.steps.min.js"></script> 

    <!--NanoScroller--> 
    <script type="text/javascript" src="js/vendors/nanoscroller/jquery.nanoscroller.min.js"></script> 

    <!--Sparkline--> 
    <script type="text/javascript" src="js/vendors/sparkline/jquery.sparkline.min.js"></script> 

    <!--Horizontal Dropdown--> 
    <script type="text/javascript" src="js/vendors/horisontal/cbpHorizontalSlideOutMenu.js"></script> 
    <script type="text/javascript" src="js/vendors/classie/classie.js"></script> 

    <!--PowerWidgets--> 
    <script type="text/javascript" src="js/vendors/powerwidgets/powerwidgets.min.js"></script> 

    <!--Bootstrap--> 
    <script type="text/javascript" src="js/vendors/bootstrap/bootstrap.min.js"></script> 

    <!--Main App--> 
    <script type="text/javascript" src="js/scripts.js"></script>



    <!--/Scripts-->

</body>
</html>