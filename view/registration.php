<!DOCTYPE HTML>
<?php
$user = @$data;
$path = implode("/", array_slice(explode("/", @$_SERVER[REQUEST_URI]), 0, -2));

?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="keywords" content="">
        <meta name="author" content="OpenS Team">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Open S Forum Front End | Registration</title>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="css/frontend.css">
        <script type="text/javascript" src="js/vendors/modernizr/modernizr.custom.js"></script>
    </head>

    <body>
        <div class="smooth-overflow frontend">

            <!--Navigation-->

            <nav class="navbar navbar-inverse" role="navigation">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1"><span class="sr-only">Toggle navigation</span> <span
                                class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
                        <a class="navbar-brand text-blue" href="<?php echo $path?>/forum/main">OpenS</a></div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                        <!--Sign In Form-->
                        <ul class="nav navbar-nav navbar-right">
                            <li id="menuLogin"><a href="<?php echo $path?>/user/login" 
                                                                   id="navLogin">Sign In</a>

                            </li>
                        </ul>
                        <!--/Sign In Form-->
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>

            <!--/Navigation-->

            <!--Breadcrumb-->
            <div class="container">
                <div class="row">
                    <div class="breadcrumb clearfix">
                        <ul>
                            <li><a href="<?php echo $path?>/forum/main"><i class="fa fa-home"></i></a></li>
                            <li><a href="#">New User</a></li>
                            <li class="active">Register</li>
                        </ul>

                        <!--Search-->
                    </div>
                </div>
            </div>
            <!--/Breadcrumb-->

            <div class="container frontend">
                <div class="row">
                    <div class="page-header">
                        <h1>Register</h1>
                    </div>

                    <!--Content-->
                    <div class="col-md-12 margin-negative-right-left-10px">
                        <div class="row bg-dark-cold-grey">
                            <div class="col-md-6">
                                <div class="registration-left-block">
                                    <h2>Join OpenS Forum</h2>
                                    <h4>ITI PHP Project</h4>

                                    <p class="lead">Join OpenS Forum to get the most recent course materials and get your
                                        knowledge to the cunning edge of the latest technology </p>
                                    <ol>
                                        <li>We are Open Source Team, We do and learn everything Open Source!
                                        </li>
                                        <li>We are great community, we have fun, play and develop everything together!
                                        </li>
                                        <li>We have such a great instructors like Mr. Islam Askar and Eng. Sayed Fathy.
                                        </li>
                                        <li>At Information Technology Institute, we develop the future of technology!
                                        </li>
                                        <li>Join us and you will not be disappointed. Yes! We promise!</li>
                                    </ol>
                                    <i class="fa fa-4x fa-plus-circle"></i>

                                    <h3>Much more!</h3>
                                    <h4>Become a part of OpenS community!</h4>
                                </div>
                            </div>
                            <!--RegForm left side ends-->
                            <div class="col-md-6 bg-cold-grey">
                                <!--Don't forget registration file.php "Yahia"-->
                                <div class="col-md-12 bootstrap-grid sortable-grid ui-sortable"> 

                                    <!-- New widget -->

                                    <div class="powerwidget blue powerwidget-sortable" id="registration-form-validation-widget" data-widget-editbutton="false" role="widget">
                                        <header role="heading">
                                            <?php echo @$user['errMsg']; ?>
                                            <div class="powerwidget-ctrls" role="menu"> <a href="#" class="button-icon powerwidget-delete-btn"><i class="fa fa-times-circle"></i></a>  <a href="#" class="button-icon powerwidget-fullscreen-btn"><i class="fa fa-arrows-alt "></i></a> <a href="#" class="button-icon powerwidget-toggle-btn"><i class="fa fa-chevron-circle-up "></i></a></div><span class="powerwidget-loader"></span></header>
                                        <div class="inner-spacer" role="content">
                                            <form action="<?php echo$path?>/user/add" id="registration-form" class="orb-form" novalidate="novalidate" method="post">
                                                <header>User Information Update</header>
                                                <fieldset>
                                                    <section>
                                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                                            Name
                                                            <input type="hidden" name="id"  value="<?php echo @$user['id'] ?>">
                                                            <input type="name" name="name" placeholder="full name" value="<?php echo @$user['name'] ?>">
                                                            <b class="tooltip tooltip-bottom-right">Needed </b> </label>
                                                    </section>
                                                    <section>
                                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                                            Username
                                                            <input type="text" name="username" placeholder="Username" value="<?php echo @$user['username'] ?>">
                                                            <b class="tooltip tooltip-bottom-right">Needed </b> </label>
                                                    </section>
                                                    <section>
                                                        <label class="input"> <i class="icon-append fa fa-code"></i>
                                                            Password
                                                            <input type="password" name="password" placeholder="password">
                                                            <b class="tooltip tooltip-bottom-right">Needed </b> </label>
                                                    </section>
                                                    <section>
                                                        <label class="input"> <i class="icon-append fa fa-code"></i>
                                                            Retype Password
                                                            <input type="password" name="confPassword" placeholder="Retype password">
                                                            <b class="tooltip tooltip-bottom-right">Needed </b> </label>
                                                    </section>
                                                    <section>
                                                        <label class="input"> <i class="icon-append fa fa-envelope-o"></i>
                                                            Email
                                                            <input type="email" name="email" placeholder="Email address" value="<?php echo @$user['email'] ?>">
                                                            <b class="tooltip tooltip-bottom-right">Needed to verify your account</b> </label>
                                                    </section>
                                                    <section>
                                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                                            Country
                                                            <input type="text" name="country" placeholder="country" value="<?php echo @$user['country'] ?>">
                                                            <b class="tooltip tooltip-bottom-right">Needed </b> </label>
                                                    </section>
                                                </fieldset>
                                                <fieldset>

                                                    <section>
                                                        <label class="select">
                                                             gender
                                                            <select name="gender">
                                                                <option value="Male">Male</option>
                                                                <option value="Female">Female</option>
                                                            </select>
                                                            <i></i> </label>
                                                    </section>
                                                    <section>
                                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                                            Signature
                                                            <input type="text" name="signature" placeholder="signature" value="<?php echo @$user['signature'] ?>">
                                                            <b class="tooltip tooltip-bottom-right">Needed </b> </label>
                                                    </section>
                                                    <!--<section>
                                                        <label class="select">
                                                            Role
                                                            <select name="role">
                                                                <option value="User">User</option>
                                                                <option value="Admin">Admin</option>
                                                            </select>
                                                            <i></i> </label>
                                                    </section>-->
                                                    <section>
                                                        <label class="select">
                                                            Profile Image
                                                            <input type="file" name="image" class="btn btn-default btn-file">
                                                            <i></i> </label>
                                                    </section>
                                                </fieldset>
                                                <footer>
                                                    <button type="submit" class="btn btn-default">Submit</button>
                                                </footer>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /End Widget --> 

                                    <!-- New widget -->


                                    <!-- /End Widget --> 

                                </div>
                            </div>
                            <!--RegForm Ends-->
                        </div>
                    </div>
                    <!--RegForm Row Ends-->
                    <div class="clearfix"></div>
                    <!-- /Content-->
                </div>
            </div>
            <!-- Page Ends -->


            <!-- scroll top -->
            <div class="scroll-top-wrapper hidden-xs"><i class="fa fa-angle-up"></i></div>
            <!-- /scroll top -->

            <!--Scripts-->
            <!--JQuery-->
            <script type="text/javascript" src="js/vendors/jquery/jquery.min.js"></script>
            <script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>

            <!--BS-->
            <script type="text/javascript" src="js/vendors/bootstrap/bootstrap.min.js"></script>

            <!--Forms-->
            <script type="text/javascript" src="js/vendors/forms/jquery.form.min.js"></script>
            <script type="text/javascript" src="js/vendors/forms/jquery.validate.min.js"></script>
            <script type="text/javascript" src="js/vendors/forms/jquery.maskedinput.min.js"></script>


            <!--Main Script-->
            <script type="text/javascript" src="js/frontend.js"></script>
    </body>
</html>
