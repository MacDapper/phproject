
﻿<!DOCTYPE html>
<?php
$path = implode("/", array_slice(explode("/", @$_SERVER[REQUEST_URI]), 0, -2)); 
?>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="keywords" content="<?php echo $data['name']?>">
    <meta name="author" content="<?php echo $data['name']?>">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $data['name']?></title>
    <link href="css/styles.css" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <script type="text/javascript" src="js/vendors/modernizr/modernizr.custom.js"></script>
</head>

<body>

    <!--Smooth Scroll-->
    <div class="smooth-overflow">
        <!--Navigation-->
        <?php include_once 'nav_menu.php'; ?>
        <!--/Navigation--> 

        <!--MainWrapper-->
        <div class="main-wrap"> 

            <!--OffCanvas Menu -->
            <aside class="user-menu"> 

                <!-- Tabs -->
                <div class="tabs-offcanvas">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#userbar-one" data-toggle="tab">Main</a></li>
                        <li><a href="#userbar-two" data-toggle="tab">Users</a></li>
                        <li><a href="#userbar-three" data-toggle="tab">ToDo</a></li>
                    </ul>
                    <div class="tab-content"> 

                        <!--User Primary Panel-->
                        <div class="tab-pane active" id="userbar-one">
                            <div class="main-info">
                                <div class="user-img"><img src="http://placehold.it/150x150" alt="User Picture" /></div>
                                <h1><?php
                                    echo @$data['LoginUser']['name'];
                                    ?> <small><?php
                                        echo @$data['loginUser']['name'];
                                        ?></small></h1>
                            </div>
                            <div class="list-group"> <a href="#" class="list-group-item"><i class="fa fa-user"></i>Profile</a> <a href="#" class="list-group-item"><i class="fa fa-cog"></i>Settings</a> <a href="#" class="list-group-item"><i class="fa fa-flask"></i>Projects<span class="badge">2</span></a>
                                <div class="empthy"></div>
                                <a href="#" class="list-group-item"><i class="fa fa-refresh"></i>Updates<span class="badge">5</span></a> <a href="#" class="list-group-item"><i class="fa fa-comment"></i>Messages<span class="badge">12</span></a> <a href="#" class="list-group-item"><i class="fa fa-comments"></i> Comments<span class="badge">45</span></a>
                                <div class="empthy"></div>
                                <a href="#" data-toggle="modal" class="list-group-item lockme"><i class="fa fa-lock"></i> Lock</a> <a data-toggle="modal" href="#" class="list-group-item goaway"><i class="fa fa-power-off"></i> Sign Out</a> </div>
                        </div>
                        <!--User Chat Panel-->
                        <div class="tab-pane" id="userbar-two">
                            <div class="chat-users-menu"> 
                                <!--Adding Some Scroll-->
                                <div class="nano">
                                    <div class="nano-content">
                                        <div class="buttons">
                                            <div class="btn-group btn-group-xs">
                                                <button type="button" class="btn btn-default">Friends</button>
                                                <button type="button" class="btn btn-default">Work</button>
                                                <button type="button" class="btn btn-default">Girls</button>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><a href="#"><span class="chat-name">Gluck Dorris</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span><span class="badge">5</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-default">Offline</span></a></li>
                                            <li><a href="#"><span class="chat-name">Mr. Joker</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Chewbacca</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">The Piggy</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Anton Durant</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-default">Offline</span></a></li>
                                            <li><a href="#"><span class="chat-name">Spiderman</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                            <li><a href="#"><span class="chat-name">Muchu</span><span class="user-img"><img src="http://placehold.it/150x150" alt="User"/></span><span class="label label-success">Online</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--User Tasks Panel-->
                        <div class="tab-pane" id="userbar-three">
                            <div class="nano"> 
                                <!--Adding Some Scroll-->
                                <div class="nano-content">
                                    <div class="small-todos">
                                        <div class="input-group input-group-sm">
                                            <input id="new-todo" placeholder="Add ToDo" type="text" class="form-control">
                                            <span class="input-group-btn">
                                                <button id="add-todo" class="btn btn-default" type="button"><i class="fa fa-plus-circle"></i></button>
                                            </span> </div>
                                        <section id="task-list">
                                            <ul id="todo-list">
                                            </ul>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /tabs --> 

            </aside>
            <!-- /Offcanvas user menu--> 

            <!--Main Menu-->

            <?php
            include_once 'left_menu.php';
            ?>
            <!--/MainMenu-->


            <!--Content Wrapper-->
            <div class="content-wrapper"> 
                <!--Horisontal Dropdown-->
                <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
                    <div class="cbp-hsinner">
                        <ul class="cbp-hsmenu">
                            <li> <a href="#"><span class="icon-bar"></span></a>
                                <ul class="cbp-hssubmenu">
                                    <li><a href="#">
                                            <div class="sparkle-dropdown"><span class="inlinebar">10,8,8,7,8,9,7,8,10,9,7,5</span>
                                                <p class="sparkle-name">project income</p>
                                                <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                                            </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="sparkle-dropdown"><span class="linechart">5,6,7,9,9,5,3,2,9,4,6,7</span>
                                                <p class="sparkle-name">site traffic</p>
                                                <p class="sparkle-amount">122541 <i class="fa fa-chevron-circle-down"></i></p>
                                            </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="sparkle-dropdown"><span class="simpleline">9,6,7,9,3,5,7,2,1,8,6,7</span>
                                                <p class="sparkle-name">Processes</p>
                                                <p class="sparkle-amount">890 <i class="fa fa-plus-circle"></i></p>
                                            </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="sparkle-dropdown"><span class="inlinebar">10,8,8,7,8,9,7,8,10,9,7,5</span>
                                                <p class="sparkle-name">orders</p>
                                                <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                                            </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="sparkle-dropdown"><span class="piechart">1,2,3</span>
                                                <p class="sparkle-name">active/new</p>
                                                <p class="sparkle-amount">500/200 <i class="fa fa-chevron-circle-up"></i></p>
                                            </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="sparkle-dropdown"><span class="stackedbar">3:6,2:8,8:4,5:8,3:6,9:4,8:1,5:7,4:8,9:5,3:5</span>
                                                <p class="sparkle-name">fault/success</p>
                                                <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                                            </div>
                                        </a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>

                <!--Breadcrumb-->
                <div class="breadcrumb clearfix">
                    <ul>
                        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active">Forum</li>
                    </ul>
                </div>
                <!--/Breadcrumb-->

                <div class="page-header">
                    <h1>Forum<small>Billboard</small></h1>

                </div>
                <!-- Widget Row Start grid -->
                <div class="row" id="powerwidgets">
                    <div class="col-md-12 bootstrap-grid"> 

                        <!-- New widget -->
                        <div class="powerwidget dark-blue" id="forum" data-widget-editbutton="false">
                            <header>
                                <h2>Forum<small>Billboard</small></h2>
                                <span><a href="<?php echo @$path ?>/category/add">
                                        <button style="position: relative;  float: right;  right: 21%;top: 13%;display: <?php echo @View::display('') ?>;padding: 5px;" type="button" class="btn btn-info">Add Category
                                    </button>
                                </a></span>
                            </header>
                            <?php for ($i = 0; $i < count(@$data['categories']); $i++) { ?>
                                <div>
                                    <div class="forum">

                                        <div class="header"><?php echo @$data['categories'][$i]['name'] ?>
                                            <span style="font-size: 12px; display: <?php echo @View::statusDisplay(@$data['categories'][$i]['status']) ?>;padding: 4px; border-radius: 12px;" 
                                                  class="alert-danger entypo-block ">This Category is currently not accept adding forums</span> 

                                            <div style="float:right;display: <?php echo @View::display('') ?>"class="control-buttons mouneer"> 
                                                <a href="<?php echo @$path . "/category/status?Notaccept&id=" . @$data['categories'][$i]['id'] ?>"
                                                   title="Ban">
                                                    <i class="fa fa-ban">

                                                    </i>
                                                </a>
                                                <a href="<?php echo @$path . "/category/status?accept&id=" . @$data['categories'][$i]['id'] ?>"
                                                   title="Activate">
                                                    <i class="fa fa-check">

                                                    </i>
                                                </a>
                                                <a href="<?php echo @$path . "/category/delete?id=" . @$data['categories'][$i]['id'] ?>" title="Delete">
                                                    <i class="fa fa-times-circle">

                                                    </i>
                                                </a> 
                                                <a href="<?php echo @$path . "/category/update?id=" . @$data['categories'][$i]['id'] ?>" title="Modify">
                                                    <i class="fa fa-cog">

                                                    </i>
                                                </a> 
                                            </div>
                                            <a href="<?php echo @$path ?>/subForum/add?categoryId=<?php echo @$data['categories'][$i]['id'] ?>"><button style="float: right;  margin-right: 5%;width: 10%; display: <?php echo @View::display('') ?>" type="button" class="btn btn-success">Add Forum
                                                </button></a>
                                            <div class="clearfix"></div>
                                        </div>                                                                                           
                                        <ul>
                                            <?php if($data['categories'][$i]['subForums']) 
                                                foreach (@$data['categories'][$i]['subForums'] as $value) {
                                                ?>
                                                <li>
                                                    <div class="col-lg-8 col-md-6 col-sm-8">
                                                        <div class="main-details"> <i class="fa fa-comments"></i>
                                                            <h3><a href="<?php echo @$path."/topic/list?forumId=".@$value['id']?>"><?php echo @$value['name']; ?></a></h3>
                                                            <span class="description">Question before you purchase? Post here and get help from us and from other clients. You can also email us for assistance.</span> 
                                                            <br>
                                                            <span style="font-weight: bolder;display: <?php echo @View::statusDisplay(@$value['status']) ?>;padding: 4px; border-radius: 12px;" 
                                                                  class="alert-danger entypo-block ">This Forum is currently not accept adding posts</span> 
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 col-md-3 visible-lg visible-md">
                                                        <div class="nums-container">
                                                            <ul class="nums">
                                                                <li>
                                                                    <div style="float:right;display: <?php echo @View::display('') ?>"class="control-buttons"> 
                                                                        <a href="<?php echo @$path . "/subForum/status?notAccept&id=" . @$value['id'] ?>"
                                                                           title="Ban">
                                                                            <i class="fa fa-ban">

                                                                            </i>
                                                                        </a>
                                                                        <a href="<?php echo @$path . "/subForum/status?Accept&id=" . @$value['id'] ?>"
                                                                           title="Activate">
                                                                            <i class="fa fa-check">

                                                                            </i>
                                                                        </a>
                                                                        <a href="<?php echo @$path . "/subForum/delete?id=" . @$value['id'] ?>" title="Delete">
                                                                            <i class="fa fa-times-circle">

                                                                            </i>
                                                                        </a> 
                                                                        <a href="<?php echo @$path . "/subForum/update?id=" . @$value['id'] ?>" title="Modify">
                                                                            <i class="fa fa-cog">

                                                                            </i>
                                                                        </a> 
                                                                    </div>
                                                                </li>
                                                                <li><strong>121</strong> Topics</li>
                                                                <li><strong>223</strong> Replies</li>
                                                                <li>
                                                                    <a href="<?php echo @$path ?>/topic/add?forumId=<?php echo @$value['id'] ?>">
                                                                        <button style="display: <?php echo @View::statusHide(@$value['status']) ?>;padding: 5px;" type="button" class="btn btn-info">Add Topic
                                                                        </button>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 col-md-3 col-sm-4 hidden-xs">
                                                        <span class="last-post"><a href="#">When It Will Be Released?</a></span>
                                                        <span class="author">by <a href="#">FoxyFox</a></span> 
                                                        <span class="badge">Today, 10:59 AM</span></div>
                                                    <div class="clearfix"></div>
                                                <?php }; ?>
                                        </ul>
                                    </div>

                                    <!--                                Need To Loop-->

                                <?php }; ?>
                            </div>

                        </div>
                        <!--/Forum Block--> 

                    </div>
                </div>
                <!-- End Widget --> 

            </div>
            <!-- /Inner Row Col-md-12 --> 
        </div>
        <!-- /Widgets Row End Grid--> 
    </div>
    <!-- / Content Wrapper --> 
</div>
<!--/MainWrapper-->
</div>
<!--/Smooth Scroll--> 


<!-- scroll top -->
<div class="scroll-top-wrapper hidden-xs">
    <i class="fa fa-angle-up"></i>
</div>
<!-- /scroll top -->



<!--Modals-->

<!--Power Widgets Modal-->

<!--Sign Out Dialog Modal-->
<div class="modal" id="signout">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="fa fa-lock"></i> </div>
            <div class="modal-body text-center">Are You Sure Want To Sign Out?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="yesigo">Ok</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<!--Lock Screen Dialog Modal-->
<div class="modal" id="lockscreen">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="fa fa-lock"></i> </div>
            <div class="modal-body text-center">Are You Sure Want To Lock Screen?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="yesilock">Ok</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<!--Scripts--> 
<!--JQuery--> 
<script type="text/javascript" src="js/vendors/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script> 

<!--Fullscreen--> 
<script type="text/javascript" src="js/vendors/fullscreen/screenfull.min.js"></script> 

<!--NanoScroller--> 
<script type="text/javascript" src="js/vendors/nanoscroller/jquery.nanoscroller.min.js"></script> 

<!--Sparkline--> 
<script type="text/javascript" src="js/vendors/sparkline/jquery.sparkline.min.js"></script> 

<!--Horizontal Dropdown--> 
<script type="text/javascript" src="js/vendors/horisontal/cbpHorizontalSlideOutMenu.js"></script> 
<script type="text/javascript" src="js/vendors/classie/classie.js"></script> 

<!--Datatables--> 
<script type="text/javascript" src="js/vendors/datatables/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="js/vendors/datatables/TableTools.min.js"></script> 
<script type="text/javascript" src="js/vendors/datatables/jquery.dataTables-bootstrap.js"></script> 
<script type="text/javascript" src="js/vendors/datatables/ZeroClipboard.js"></script> 
<script type="text/javascript" src="js/vendors/datatables/dataTables.colVis.js"></script> 
<script type="text/javascript" src="js/vendors/datatables/colvis.extras.js"></script> 

<!--PowerWidgets--> 
<script type="text/javascript" src="js/vendors/powerwidgets/powerwidgets.min.js"></script> 

<!--Bootstrap--> 
<script type="text/javascript" src="js/vendors/bootstrap/bootstrap.min.js"></script> 

<!--ToDo--> 
<script type="text/javascript" src="js/vendors/todos/todos.js"></script> 

<!--Main App--> 
<script type="text/javascript" src="js/scripts.js"></script>



<!--/Scripts-->

</body>
</html>
<!---->
