﻿<!DOCTYPE html>
<?php
$path = implode("/", array_slice(explode("/", @$_SERVER[REQUEST_URI]), 0, -2));

?>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OpenS Forum | Login</title>
    <link href="css/styles.css" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <script type="text/javascript" src="js/vendors/horisontal/modernizr.custom.js"></script>
</head>

<body>
    <div class="colorful-page-wrapper">
        <div class="center-block">
            <div class="login-block">
                <!--Add action file.php "Yahia"-->
                <form action="<?php echo $path?>/user/login" method="post" id="login-form" class="orb-form" name="loginForm" accept-charset="utf-8">
                    <header>
                        <div class="image-block"><a href="<?php echo $path?>/forum/main"><img src="images/logo.png" alt="User" /></a></div>
                        Login to OpenS Forum <small>Have no account? &#8212; <a href="<?php echo $path?>/user/register">Register</a></small></header>
                    <fieldset>
                        <section>
                            <div class="row">
                                <label class="label col col-4">E-mail</label>
                                <div class="col col-8">
                                    <label class="input"> <i style="
                                                             margin-top: 0px;
                                                             " class="icon-append fa fa-user"></i>
                                        <input type="email" name="email" />
                                    </label>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="row">
                                <label class="label col col-4">Password</label>
                                <div class="col col-8">
                                    <label class="input"> <i style="
                                                               margin-top: 0px;
                                                             " class="icon-append fa fa-lock"></i>
                                        <input type="password" name="password" />
                                    </label>
                                    <div class="note"><a href="#">Forgot password?</a></div>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="row">
                                <div class="col col-4"></div>
                                <div class="col col-8">
                                    <label class="checkbox">
                                        <input type="checkbox" name="remember" >
                                        <i></i>Keep me logged in</label>
                                    <?php echo @$data['errMsg']?>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" name="submit" class="btn btn-default">Log in</button>
                    </footer>
                </form>
            </div>

            <div class="copyrights"> Open Source Forum <br>
                Created by Open Source Team &copy; 2015 </div>
        </div>
    </div>

    <!--Scripts--> 
    <!--JQuery--> 
    <script type="text/javascript" src="js/vendors/jquery/jquery.min.js"></script> 
    <script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script> 

    <!--Forms--> 
    <script type="text/javascript" src="js/vendors/forms/jquery.form.min.js"></script> 
    <script type="text/javascript" src="js/vendors/forms/jquery.validate.min.js"></script> 
    <script type="text/javascript" src="js/vendors/forms/jquery.maskedinput.min.js"></script> 
    <script type="text/javascript" src="js/vendors/jquery-steps/jquery.steps.min.js"></script> 

    <!--NanoScroller--> 
    <script type="text/javascript" src="js/vendors/nanoscroller/jquery.nanoscroller.min.js"></script> 

    <!--Sparkline--> 
    <script type="text/javascript" src="js/vendors/sparkline/jquery.sparkline.min.js"></script> 

    <!--Main App--> 
    <script type="text/javascript" src="js/scripts.js"></script>



    <!--/Scripts-->

</body>
</html>