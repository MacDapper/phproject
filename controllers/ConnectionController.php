<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConnectionController
 *
 * @author omnia
 */
include_once "./model/Connection.php";
class ConnectionController {
    //put your code here
    public function __construct($host, $user, $password, $db) {
        $this->connection = new Connection($host, $user, $password, $db);
    }
    public function getConnection() {
        return $this->connection;
    }
    public function __destruct() {
        //$this->connection->__destruct();
    }
}
