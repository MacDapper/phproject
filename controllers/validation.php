<?php

class Validator {

    const REGEX_EMAIL = '/^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$/i';
    const REGEX_ALPHA = '/^[A-Z.]+$/i';
    const REGEX_ALPHA_NUM = '/^[A-Z0-9._+-]+$/i';
    const ERROR_REQUIRED = 'This field is required. Please enter a value.';
    const ERROR_ALPHA = 'This field should contain only alphabetical characters';
    const ERROR_ALPHA_NUM = 'This field should contain both letters and numbers';
    const ERROR_EMAIL = 'Please input a valid email address.';
    const ERROR_MIN_LENGTH = 'This field does not meet the minimum length.';
    const ERROR_MAX_LENGTH = 'This field exceeds the maximum length.';
    const ERROR_NUMERIC = 'This field should hold a numeric value.';
    const ERROR_PASSWORD = 'Passwords didn\'t match';

    private $fields = array();
    private $rule = array();
    private $errors = array();

    function __construct($form_data, $validate_rule) {
        $this->fields = $this->sanitize($form_data);
        $this->rules = $validate_rule;
    }

    private function sanitize($form_data) {
        $sanitized_data = filter_var_array($form_data, FILTER_SANITIZE_STRING);

        // Return the sanitized datas
        return $sanitized_data;
    }

    private function email($field, $value) {
        if (!preg_match(self::REGEX_EMAIL, $value)) {
            $this->errors[$field] = self::ERROR_EMAIL;
        }
    }

    private function match_password($field, $password, $confPassword) {
        if ($password !== $confPassword) {
            $this->errors[$field] = self::ERROR_PASSWORD;
        }
    }

    private function alpha($field, $value) {
        if (!preg_match(self::REGEX_ALPHA, $value)) {
            $this->errors[$field] = self::ERROR_ALPHA;
        }
    }

    private function alpha_num($field, $value) {
        if (!preg_match(self::REGEX_ALPHA_NUM, $value)) {
            $this->errors[$field] = self::ERROR_ALPHA_NUM;
        }
    }

    private function numeric($field, $value) {
        if (!is_numeric($value)) {
            $this->errors[$field] = self::ERROR_NUMERIC;
        }
    }

    private function min_length($field, $value, $min_length) {
        $length = strlen($value);

        if ($length < $min_length) {
            $this->errors[$field] = self::ERROR_MIN_LENGTH;
        }
    }

    private function max_length($field, $value, $max_length) {
        $length = strlen($value);

        if ($length > $max_length) {
            $this->errors[$field] = self::ERROR_MAX_LENGTH;
        }
    }

    public function get_fields() {
        return $this->fields;
    }

    public function get_errors() {
        foreach ($this->errors as $field => $err)
            $errMsg.="$field: $err <br/>";
        return $errMsg;
    }

    public function get_errors_json() {
        return json_encode($this->errors);
    }

    public function validate() {

        foreach ($this->fields as $field => $value) {
            // If the field value is empty
            if (empty($value)) {
                // If the field is set as required, throw error
                if (isset($this->rules[$field]['required'])) {
                    $this->errors[$field] = self::ERROR_REQUIRED;
                }
            }

            // Else, if the field has a value and is declared in Rules
            else if (isset($this->rules[$field])) {

                unset($this->rules[$field]['required']);

                foreach ($this->rules[$field] as $rule => $rule_value) {
                    call_user_func_array(
                            array($this, $rule), array($field, $value, $rule_value)
                    );
                }
            }
        }

        // Return validation result
        if (empty($this->errors)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
