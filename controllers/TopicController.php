<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author omnia
 */
//function __autoload($class_name) {

include_once "./model/TopicModel.php";
include_once "./model/ReplyModel.php";
include_once './model/View.php';
//include_once './model/CategoryModel.php';
include_once './model/SubForumModel.php';
include_once './controllers/validation.php';

//        include_once  $class_name . '.php';
//  }

class TopicController {

    //put your code here


    public function __construct($conn, $table) {
        /* Initialize action controller here */
        $this->conn = $conn;
        $this->topicModel = new TopicModel($conn, $table);
        $this->path = "$_SERVER[DOCUMENT_ROOT]/$_SERVER[REQUEST_URI]";
        $this->validation_rules = array(
            'title' => array(
                'required' => true,
                'alpha' => true,
                'alpha_num' => true,
                'min_length' => 2
            ),
            'body' => array(
                'required' => true,
                'alpha' => true,
                'alpha_num' => true,
                'min_length' => 2
            ),
            'status' => array(
                'required' => true,
            ),
            'stickiness' => array(
                'required' => true,
            ),
            'forumId' => array(
                'required' => true,
            ),
            'userId' => array(
                'required' => true,
            ),
        );
    }

    public function listTopicAction() {
        //////////here//////////
        $subForumModel = new SubForumModel($this->conn, 'forum');
        $userModel = new UserModel($this->conn, 'user');
        //////////here////////// 
        $data['forumStatus'] = $subForumModel->getSubForumStatus($_GET['forumId']);
        $data['forumName'] = $subForumModel->getSubForumName($_GET['forumId']);
        $data['topics'] = $this->topicModel->getTopicsBySubForumId($_GET['forumId']);
        //foreach will not work
        for ($i = 0; $i < count($data['topics']); $i++) {
            $data['topics'][$i]['userName'] = $userModel->getUserById($data['topics'][$i]['userId'])[0]['name'];
            $data['topics'][$i]['forumName'] = $subForumModel->getSubForumName($data['topics'][$i]['forumId']);
        }
        $data['forumId'] = $_GET['forumId'];

        echo View::render2("./view/admin-topics-list.php", $data);
    }

    public function addTopicAction() {
        //$this->topicModel->getTopicById()[0];
        $subForumModel = new SubForumModel($this->conn, 'forum');
        if (($subForumModel->getSubForumStatus($_GET['forumId']) == "Accept" &&
                $_SESSION['loginUser']['status'] == "Active") ||
                $_SESSION['loginUser']['role'] == "Admin") {
            switch ($_SERVER['REQUEST_METHOD']) {
                case 'GET':
                    $data['forumId'] = $_GET['forumId'];
                    echo View::render2("./view/admin-add-topic.php", $data);
                    break;
                case 'POST':
                    $this->saveTopicAction();
                    break;
            }
        } else {
            echo View::redirect("forum/main");
        }
    }

    public function updateTopicAction() {
        $topic = $this->topicModel->getTopicById($_GET['id'])[0];
        if (($topic['userId'] == $_SESSION['loginUser']['id'] &&
                $_SESSION['loginUser']['status'] == "Active") ||
                $_SESSION['loginUser']['role'] == "Admin") {
            switch ($_SERVER['REQUEST_METHOD']) {
                case 'GET':
                    echo View::render2("./view/admin-add-topic.php", $topic);
                    break;
                case 'POST':
                    $data = array(
                        'title' => $_POST['title'],
                        'body' => $_POST['body'],
                        'status' => $_POST['status'],
                        'stickiness' => $_POST['stickiness'],
                    );
                    $validator = new Validator($data, $this->validation_rules);
                    if ($validator->validate()) {
                        if (($err = $this->topicModel->updateTopicById($_POST['id'], $data)) == 1) {
                            echo View::redirect("topic/list?forumId=" . $_POST['forumId']);
                        } else {
                            $data = $topic;
                            $data['errMsg'] = $err;
                        }
                    } else {
                        @$data['errMsg'] = $validator->get_errors();
                        @$data['forumId'] = $topic['forumId'];
                        @$data['id'] = $topic['id'];
                        echo View::render2("./view/admin-add-topic.php", $data);
                    }
                    break;
            }
        } else {
            echo View::redirect("forum/main");
        }
    }

    public function saveTopicAction() {
        $data = array(
            'id' => "",
            'title' => $_POST['title'],
            'body' => $_POST['body'],
            'status' => "Accept",
            'stickiness' => "Not Sticky",
            'forumId' => $_POST['forumId'],
            'userId' => $_POST['userId'],
        );
        $validator = new Validator($data, $this->validation_rules);
        if ($validator->validate()) {
            if (($err = $this->topicModel->addTopic($data)) == 1) {
                echo View::redirect("topic/list?forumId=" . $_POST['forumId']);
            } else {
                $data['errMsg'] = $err;
            }
        } else {
            @$data['errMsg'] = $validator->get_errors();
        }
        $data['forumId'] = $_POST['forumId'];
        echo View::render2("./view/admin-add-topic.php", $data);
        return;
    }

    function deleteTopicAction($id) {
        $topic = $this->topicModel->getTopicById($id)[0];
        if (($topic['userId'] == $_SESSION['loginUser']['id'] &&
                $_SESSION['loginUser']['status'] == "Active") ||
                $_SESSION['loginUser']['role'] == "Admin") {
            if (isset($topic)) {
                $this->topicModel->deleteTopic($id);
                echo View::redirect("topic/list?forumId=" . $topic['forumId']);
            } else
                echo View::redirect("forum/main");
        }else {
            echo View::redirect("forum/main");
        }
    }

    function viewTopicAction($id, $err) {

        $topic = $this->topicModel->getTopicById($id)[0];
        $topic['errMsg'] = $err;
        ///////////////here//////////////
        include_once "controllers/ReplyController.php";
        $this->replyController = new ReplyController($this->conn, "reply");
        ///////////////here//////////////
        $topic['replies'] = $this->replyController->listReplyAction($id);
        echo View::render2("./view/admin-topic-view.php", $topic);
    }

}
