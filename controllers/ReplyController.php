<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author omnia
 */
//function __autoload($class_name) {

include_once "./model/ReplyModel.php";
include_once './model/View.php';

////include_once './model/CategoryModel.php';
//        include_once  $class_name . '.php';
//  }
class ReplyController {

    //put your code here


    public function __construct($conn, $table) {
        /* Initialize action controller here */
        ///////////////here//////////////
        include_once "controllers/TopicController.php";
        $topicController = new TopicController($conn, "topic");
        $this->conn = $conn;
        ///////////////here//////////////
        $this->replyModel = new ReplyModel($conn, $table);
        $this->path = "$_SERVER[DOCUMENT_ROOT]/$_SERVER[REQUEST_URI]";
    }

    public function listReplyAction($topicId) {
        //////////here//////////
        $userModel = new UserModel($this->conn, 'user');
        //////////here////////// 
        $replies = $this->replyModel->getRepliesByTopicId($topicId);
        //foreach will not work
        for ($i = 0; $i < count($replies); $i++) {
            $replies[$i]['userName'] = $userModel->getUserById($replies[$i]['userId'])[0]['name'];
        }
        return $replies;
    }

    public function addReplyAction() {
        //////////here//////////
        $topicModel = new TopicModel($this->conn, 'topic');
        //////////here////////// 
        if (($topicModel->getTopicById($_POST['topicId'])[0]['status'] == "Accept" && $_SESSION['loginUser']['status'] == "Active") || $_SESSION['loginUser']['role'] == "Admin") {
            $data = array(
                'id' => "",
                'body' => $_POST['body'],
                'topicId' => $_POST['topicId'],
                'userId' => $_POST['userId'],
            );
            if (($err = $this->replyModel->addReply($data)) == 1) {
                echo View::redirect("topic/view?id=" . $_POST['topicId']);
            } else {
                $topic->viewTopicAction($_POST['topicId'], $err);
            }
        } else {
            echo View::redirect("forum/main");
        }
    }

    public function updateReplyAction() {
        if (($this->replyModel->getReplyById($_POST['id'])[0]['userId'] == $_SESSION['loginUser']['id'] && $_SESSION['loginUser']['status'] == "Active") || $_SESSION['loginUser']['role'] == "Admin") {
            $data = array(
                'body' => $_POST['body'],
            );
            if (($err = $this->replyModel->updateReplyById($_POST['id'], $data)) == 1) {
                echo View::redirect("topic/view?id=" . $_POST['topicId']);
            } else {
                $data = $topic;
                $data['errMsg'] = $err;
                $topic->viewTopicAction($_POST['topicId'], $err);
            }
        } else {
            echo View::redirect("forum/main");
        }
    }

    function deleteReplyAction($id) {
        if (($this->replyModel->getReplyById($_GET['id'])[0]['userId'] == $_SESSION['loginUser']['id'] 
                && $_SESSION['loginUser']['status'] == "Active")
                || $_SESSION['loginUser']['role'] == "Admin") {
            $reply = $this->replyModel->getReplyById($id)[0];
            if (isset($reply)) {
                $this->replyModel->deleteReply($id);
                echo View::redirect("topic/view?id=" . $reply['topicId']);
            } else
                echo View::redirect("forum/main");
        } else {
            echo View::redirect("forum/main");
        }
    }

}
