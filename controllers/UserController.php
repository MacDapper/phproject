<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author omnia
 */
//function __autoload($class_name) {

include_once "./model/UserModel.php";
include_once './model/View.php';
include_once 'validation.php';

//include_once './model/CategoryModel.php';
//include_once './model/SubForumModel.php';
//        include_once  $class_name . '.php';
//  }
class UserController {

    //put your code here


    public function __construct($conn, $table) {
        /* Initialize action controller here */

        $this->userModel = new UserModel($conn, $table);
        $this->path = "$_SERVER[DOCUMENT_ROOT]/$_SERVER[REQUEST_URI]";
        $this->validation_rules = array(
            'name' => array(
                'required' => true,
                'alpha' => true,
                'min_length' => 2
            ),
            'username' => array(
                'required' => true,
                'alpha' => true,
                'alpha_num' => true,
                'min_length' => 2
            ),
            'password' => array(
                'required' => true,
                'min_length' => 6,
                'alpha_num' => true
            ),
            'confPassword' => array(
                'required' => true,
                'alpha_num' => true,
                'match_password' => true
            ),
            'email' => array(
                'required' => true,
                'email' => true
            ),
            'country' => array(
                'required' => true,
                'min_length' => 2,
                'alpha' => true,
            ),
            'gender' => array(
                'required' => true,
                'alpha' => true,
            ),
            'signature' => array(
                'required' => true,
                'min_length' => 2,
                'alpha' => true,
                'alpha_num' => true,
            ),
            'image' => array(
                'required' => true,
            ),
        );
    }

    public function loginAction($data) {
        $viewPath = "./view/admin-login.php";
        echo View::render2($viewPath, $data);
    }

    public function isValidAction($email, $password) {
        $user = $this->userModel->getUserByEmail($email, $password)[0];

        if (isset($user) && $user['password'] === md5($password)) {
            //check if user is active or not so can't login
            switch ($user['status']) {
                case "Active":
                    unset($user["password"]);
                    $_SESSION['loginUser'] = $user;
                    echo View::redirect("forum/main");
                    return;
                    break;
                case "Inactive":
                    $this->loginAction(array("errMsg" => "You are bannded"));
                    break;
            }
        } else {
            $this->loginAction(array("errMsg" => "Invalid email or password."));
        }
    }

    public function profileAction($id) {
        $user = $this->userModel->getUserById($id)[0];
        if (isset($user)) {
            unset($user["password"]);
//           var_dump($user);
            echo View::render2("./view/admin-profile.php", $user);
        }
    }

    public function logoutAction($id) {
        unset($_SESSION['loginUser']);
        View::redirect("forum/main");
    }

    public function listUserAction() {
        $users = $this->userModel->listUsers();
        echo View::render2("./view/admin-user-list.php", $users);
    }

//used for add user by admin or update any by himself 
    public function registerAction($id) {
        //must have session to enter here
        //if admin so can update any user
        //if user update him self only
        $user = array();
        if ($_SESSION['loginUser']['role'] == 'Admin') {
            //if admin updates user so fetch this user from th DB else return 
            //empty user 
            if (!empty($id)) {
                $user = $this->userModel->getUserById($id)[0];
                echo View::render2("./view/admin-user-update.php", $user);
            } else {
                echo View::render2("./view/registration.php", array());
            }
            return;
        }
        //normal user updates only his data
        else {

            if (!empty($id)) {
                $user = $this->userModel->getUserById($_SESSION['loginUser']['id'])[0];
                echo View::render2("./view/admin-user-update.php", $user);
                return;
            } elseif (!$_SESSION['loginUser'])
                echo View::redirect('view/registration.php');
            else {
                View::redirect("forum/main");
            }
        }
//        if (isset($_SESSION['loginUser']))
//        else {
//            echo View::redirect("./view/registration.php", $user);
//        }
    }

//admin add new user to DB
    public function saveUserAction() {
//        if (!isset($_POST['role']))
//            $_POST['role'] = "User";
//        if (!isset($_POST['status']))
//            $_POST['status'] = "Active";
        $data = array(
            'id' => "",
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'username' => $_POST['username'],
            'country' => $_POST['country'],
            'gender' => $_POST['gender'],
            'signature' => $_POST['signature'],
            'role' => $_POST['role'],
            'status' => $_POST['status']
        );
        if (isset($_POST['password'])&&!empty($_POST['password']))
            $data['password'] = md5($_POST['password']);
        $validator = new Validator($data, $this->validation_rules);
        if ($validator->validate()) {
            if (($err = $this->userModel->addUser($data)) == 1) {
                echo View::redirect("user/list");
                unset($data['password']);
                Mail::sendMail($data);
            } else {
                @$data['errMsg'] = $err;
            }
        } else {
            @$data['errMsg'] = $validator->get_errors();
        }
        if (isset($_POST['id']) && !empty($_POST['id']) || isset($_SESSION['loginUser']))
            echo View::render2("./view/admin-user-update.php", $data);
        else
            echo View::render2("./view/registration.php", $data);
        return;
    }

    public function userUpdateAction() {
        $data = array(
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'username' => $_POST['username'],
            'country' => $_POST['country'],
            'gender' => $_POST['gender'],
            'signature' => $_POST['signature'],
            'role' => $_POST['role'],
            'status' => $_POST['status']
        );
        if (isset($_POST['password']) && !empty($_POST['password'])) {
            $data['password'] = $_POST['password'];
        }
        $this->userModel->updateUserById($_POST['id'], $data);
        //$this->profileAction($_POST['id']);
        echo View::redirect("user/view?id=" . $_POST['id'], $user);
    }

    function deleteUser($id) {
        $user = $this->userModel->getUserById($id)[0];
        if (isset($user)) {
            $this->userModel->deleteUser($id);
        }
        $this->listUserAction();
    }

}
