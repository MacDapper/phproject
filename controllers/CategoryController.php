<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoryController
 *
 * @author omnia
 */
include_once './model/CategoryModel.php';
include_once 'validation.php';

class CategoryController {

    //put your code here
    public function __construct($connection, $table) {
        $this->categoryModel = new CategoryModel($connection, $table);
        $this->validation_rules = array(
            'name' => array(
                'required' => true,
                'alpha' => true,
                'min_length' => 2
            ),
            'status' => array(
                'required' => true,
            ),
            'forumId' => array(
                'required' => true,
            )
        );
    }

    //for add new category and update one
    public function CategoryForm() {
        if ($_SESSION['loginUser']['role'] === 'Admin') {
            $category = array();
            if (isset($_GET['id'])) {
                $category = $this->categoryModel->getCategoryById($_GET['id'])[0];
            }

            echo View::render2('./view/admin-add-category.php', $category);
        } else {
            View::redirect('forum/main');
        }
    }

    public function SaveCategory() {
        $cat = array(
            'name' => $_POST['name'],
            'status' => $_POST['status'],
            'forumId' => $_POST['forumId']
        );
        $validator = new Validator($_POST, $this->validation_rules);
        if ($validator->validate()) {
            if (empty($_POST['id'])) {
                if (($err = $this->categoryModel->addCategory($cat)) != 1)
                    @$data['errMsg'] = $err;
            } else {
                if (($err = $this->categoryModel->updateCategoryById($_POST['id'], $cat)) != 1)
                    @$data['errMsg'] = $err;
            }
        } else {
            @$data['errMsg'] = $validator->get_errors();
        }
        if (empty($data['errMsg']))
            View::redirect('forum/main');
        else {
            $data['id'] = @$_POST['id'];
            echo View::render2('./view/admin-add-category.php', $data);
        }
        return;
    }

    public function deleteCategory() {
        if ($_SESSION['loginUser']['role'] === 'Admin' && isset($_GET['id']))
            $this->categoryModel->deleteCategory($_GET['id']);
        View::redirect('forum/main');
    }

//    public function updateCategory() {
//        
//    }
}
