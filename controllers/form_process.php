<?php


$errors = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $validator = new Validator($_POST, $validation_rules);

    if ($validator->validate()) {

        $clean_data = $validator->get_fields();

        // To-do (Write email code here to send it ot user)
        
        $result = array(
            'success' => array('Thank you for your registration, You should receive an email from us by now'));

        echo json_encode($result);
    }
    else {
        $errors = $validator->get_errors_json();

        echo $errors;
    }
}

