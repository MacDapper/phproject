<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SubForumController
 *
 * @author omnia
 */
include_once './model/SubForumModel.php';
include_once 'validation.php';

class SubForumController {

    //put your code here
    public function __construct($connection, $table) {
        $this->subForumModel = new SubForumModel($connection, $table);
        $this->validation_rules = array(
            'name' => array(
                'required' => true,
                'alpha' => true,
                'min_length' => 2
            ),
            'status' => array(
                'required' => true,
            ),
            'categoryId' => array(
                'required' => true,
            )
        );
    }

    //for add new subForum and update one
    public function SubForumForm() {
        if ($_SESSION['loginUser']['role'] === 'Admin') {
            $subForum = array();
            $subForum['categoryId'] = $_GET['categoryId'];
            if (isset($_GET['id'])) {
                $subForum = $this->subForumModel->getSubForumById($_GET['id'])[0];
            }
            echo View::render2('./view/admin-forum-add.php', $subForum);
        } else {
            View::redirect('forum/main');
        }
    }

    public function SaveSubForum() {
        $subForum = array(
            'name' => $_POST['name'],
            'status' => $_POST['status'],
            'categoryId' => $_POST['categoryId']
        );
        $validator = new Validator($_POST, $this->validation_rules);
        if ($validator->validate()) {
            if (empty($_POST['id'])) {
                if (($err = $this->subForumModel->addSubForum($subForum)) != 1)
                    @$subForum['errMsg'] = $err;
            } else {
                if (($err = $this->subForumModel->updateSubForumById($_POST['id'], $subForum)) != 1)
                    @$subForum['errMsg'] = $err;
            }
        } else {
            @$subForum['errMsg'] = $validator->get_errors();
        }
        if (empty(@$subForum['errMsg']))
            View::redirect('forum/main');
        else {
            @$subForum['id'] = @$_POST['id'];
            echo View::render2('./view/admin-forum-add.php', @$subForum);
        }
        return;
    }

    public function deleteSubForum() {
        if ($_SESSION['loginUser']['role'] === 'Admin' && isset($_GET['id']))
            $this->subForumModel->deleteSubForum($_GET['id']);
        View::redirect('forum/main');
    }

    public function listSubForumsByCategoryId($categoryId) {
        return $this->subForumModel->getSubForumsByCategoryId($categoryId);
    }

}
