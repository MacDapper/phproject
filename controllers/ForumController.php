<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MainForumController
 *
 * @author omnia
 */
include_once './model/View.php';
include_once './model/ForumModel.php';
include_once './model/CategoryModel.php';
include_once './model/SubForumModel.php';

class ForumController {

    //put your code here
    public function __construct($connection, $table, $name) {
        /* Initialize action controller here */
        $this->forumModel = new ForumModel($connection, $table, $name);
        $this->categoryModel = new CategoryModel($connection, 'category');
        $this->subForumModel = new SubForumModel($connection, 'forum');
        $this->path = "$_SERVER[DOCUMENT_ROOT]/$_SERVER[REQUEST_URI]";
    }

    public function main() {
        if (!($this->forumModel->getForumStatus() === "Not Working" && $_SESSION['loginUser']['role'] === 'User')) {
            $forum['name'] = $this->forumModel->getForumName();
            $categories = $this->categoryModel->listCategories();
            for ($i = 0; $i < count($categories); $i++) {
                $categories[$i]['subForums'] = $this->subForumModel
                        ->getSubForumsByCategoryId($categories[$i]['id']);
            }
            $forum['categories'] = $categories;
            $forum['status'] = $this->forumModel->getForumStatus();
            echo View::render2("./view/admin-forum.php", $forum);
        }
    }

//    change to the opposite the forum status
    public function toggleStatus() {
        if ($_SESSION['loginUser']['role'] === 'Admin')
            switch ($this->forumModel->getForumStatus()) {
                case "Working":
                    $this->forumModel->setForumNotWorking();
                    break;
                case "Not Working":
                    $this->forumModel->setForumWorking();
            }
        View::redirect('forum/main');
    }

//    //forum view?
//    //work based on just change the forum name 
//    public function saveUpdate() {
//        if($_SESSION['loginUser']['role']==='admin' 
//                && isset($_POST['name']) && !empty($_POST['name']))
//            $this->forumModel->changeForumName($_POST['name']);
//    }
//    
//    //forum view
    public function getStatus() {
        return $this->forumModel->getForumStatus();
    }
}
