<?php

include_once 'Model.php';

abstract class StandardForumModel extends Model {

    function __construct($conn, $tbName) {
        parent::__construct($conn, $tbName);
    }

    function intiate($ForumName, $categoryId, $status) {
        $data["name"] = $ForumName;
        $data["status"] = $status;
        if ($categoryId)
            $data["categoryId"] = $categoryId;
        return $this->insert($data);
    }

    function addForum($data) {
        return $this->insert($data);
    }

    function updateForumById($id,$data) {
        return $this->update($data, "id=$id");
    }

    function changeStatus($id, $ForumStatus) {
        $data["status"] = $ForumStatus;
        return $this->update($data, "id=$id");
    }

    function changeName($id, $ForumName) {
        $data["name"] = $ForumName;
        return $this->update($data, "id=$id");
    }

    function getStatus($id) {
        $forum = $this->select("id=$id");
        return $forum[0]['status'];
    }

    function getName($id) {
        $forum = $this->select("id=$id");
        return $forum[0]['name'];
    }

    function getAll() {
        $forum = $this->selectAll();
        return $forum;
    }

    function deleteForum($id) {
        return $this->delete("id=$id");
    }

}
