<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Connection
 *
 * @author mahmoud
 */
include_once "./model/Connection.php";
class Connection {
    //put your code heret
    
    function __construct($host, $username, $passord, $dbName) {
         $this->db = mysqli_connect($host, $username, $passord, $dbName);
        if (mysqli_connect_errno()) {
            echo 'Error: Could not connect to database. Please try again later.';
            exit;
        }
    }

    function getConnection() {
        return $this->db;
    }

    function __destruct() {
        //mysqli_close($this->db);
    }
}
