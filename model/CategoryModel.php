<?php

class CategoryModel extends Model {

    function __construct($conn, $tbName) {
        parent::__construct($conn, $tbName);
    }

    function addCategory($data) {
//        $data['name']=$name;
//        $data['status']="Accept";
//        $data['forumId']=1;
        return $this->insert($data);
    }

    function deleteCategory($id) {
        return $this->delete("id=$id");
    }

    function listCategories() {
        return $this->selectAll();
    }

    function getCategoryById($id) {
        return $this->select("id=$id");
    }

    function updateCategoryById($id, $data) {
        return $this->update($data, "id=$id");
    }

    function getCategoryName($id) {
        return $this->select("id=$id")[0]["name"];
    }

    function changeCategoryName($id, $name) {
        $data['name'] = $name;
        return $this->update($data, "id=$id");
    }

    function setCategoryStatusAccept($id) {
        $data['status'] = "Accept";
        return $this->update($data, "id=$id");
    }

    function setCategoryStatusNotAccept($id) {
        $data['status'] = "Not Accept";
        return $this->update($data, "id=$id");
    }

    function getCategoryStatus($id) {
        return $this->select("id=$id")[0]['status'];
    }

    function getCategoryForumId($id) {
        return $this->select("id=$id")[0]['forumId'];
    }

}

?>