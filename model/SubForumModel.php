<?php

// include 'StandardForumModel.php';

class SubForumModel extends StandardForumModel {

    function __construct($conn, $tbName) {
        parent::__construct($conn, $tbName);
    }

    function setSubForumNotAccept($id) {
        return $this->changeStatus($id, "Not Accept");
    }

    function setSubForumAccept($id) {
        return $this->changeStatus($id, "Accept");
    }

    function changeSubForumName($id, $subForumName) {
        return $this->changeName($id, $subForumName);
    }

    function getSubForumStatus($id) {
        return $this->getStatus($id);
    }

    function getSubForumName($id) {
        return $this->getName($id);
    }

    function addSubForum($data) {
        return $this->addForum($data);
    }

    function updateSubForumById($id,$data) {
        return $this->updateForumById($id,$data);
    }

    function getAllSubForums() {
        $subForums = $this->getAll();
        return $subForums;
    }

    function getSubForumsByCategoryId($categoryId) {
        return $this->select("categoryId=$categoryId");
    }

    function getSubForumById($id) {
        return $this->select("id=$id");
    }

    function deleteSubForum($id) {
        $this->deleteForum($id);
    }

}

?>