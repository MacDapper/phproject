<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author mahmoud
 */
class Model {
    //put your code here
    function __construct($conn, $tbName) {
        $this->tbName = $tbName;
        $this->conn = $conn->connection->db;
    }

    function select($condition) {
        $query = "select * from " . $this->tbName . " where " . $condition;
        $result = mysqli_query($this->conn, $query);
        $this->num_results = mysqli_num_rows($result);
        for ($i = 0; $i < $this->num_results; $i++)
            $output[] = mysqli_fetch_assoc($result);
        if (isset($output))
            return $output;
    }

    function delete($condition) {
        $query = "delete from " . $this->tbName . " where " . $condition;
        $result = mysqli_query($this->conn, $query);
        $this->num_results = mysqli_affected_rows($this->conn);
        return $this->num_results;
    }

    function selectAll() {
        $query = "select * from " . $this->tbName;
        $result = mysqli_query($this->conn, $query);
        $this->num_results = mysqli_num_rows($result);
        for ($i = 0; $i < $this->num_results; $i++)
            $output[] = mysqli_fetch_assoc($result);
        if (isset($output))
            return $output;
    }

    function insert($data) {
        $subQuery = "";
        foreach ($data as $key => $value)
            $subQuery .=$key . "='" . $value . "',";
        $subQuery = rtrim($subQuery, ",");
        $query = "insert into " . $this->tbName . " set " . $subQuery;
        $result = mysqli_query($this->conn, $query);
        if ($result)
            return $result;
        else {
            //echo mysqli_errno($this->conn) . ": " . mysqli_error($this->conn) . "\n";
            echo $query."<br/>";
                    echo mysqli_error($this->conn);
        exit();
        }
    }

    function update($data, $condition) {
        $subQuery = "";
        foreach ($data as $key => $value)
            $subQuery .=$key . "='" . $value . "',";
        $subQuery = rtrim($subQuery, ",");
        $query = "update " . $this->tbName . " set " . $subQuery . " where " . $condition;
        $result = mysqli_query($this->conn, $query);
        if ($result==true)
            return 1;
        $this->num_results = mysqli_affected_rows($this->conn);
        if ($this->num_results > 0)
            return $this->num_results;
        elseif ($this->num_results == -1)
            echo mysqli_errno($this->conn) . ": " . mysqli_error($this->conn) . "\n";
    }

}
