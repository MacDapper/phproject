<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ReplyModel extends Model {

    function __construct($conn, $tbName) {
        parent::__construct($conn, $tbName);
    }

    function addReply($data) {
        return $this->insert($data);
    }

    function deleteReply($id) {
        return $this->delete("id=$id");
    }

    function getRepliesByTopicId($topicId) {
        return $this->select("topicId=$topicId");
    }

    function getRepliesByUserId($userId) {
        return $this->select("userId=$userId");
    }

    function listReplies() {
        return $this->selectAll();
    }

    function getReplyById($id) {
        return $this->select("id=$id");
    }

    function updateReplyById($id, $data) {
        return $this->update($data, "id=$id");
    }

}

?>