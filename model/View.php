<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of View
 *
 * @author mahmoud
 */
class View {

    //put your code here
    static function render($path, $input) {
        $url = $path;
        $data = $input;
        $content = http_build_query($data);

        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => $content,
            ),
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, FALSE, $context, 0);
        return $result;
    }

    static function render2($path, $data) {

        ob_start(); // start output buffer
        extract($data); // see PHPDoc
        // from here $var1 will be accessible with value "value1"
        // so your template may contain references to $var1

        include $path;
        $template = ob_get_contents(); // get contents of buffer
        ob_end_clean();
        return $template;
    }

    static function redirect($path) {
        header("Location: ../$path");
    }

    static function display($id) {
        if (isset($_SESSION['loginUser']) && ($_SESSION['loginUser']['id'] == $id || $_SESSION['loginUser']['role'] == 'Admin'))
            return "inline";
        return "none";
    }

    static function hide($id) {
        if ($_SESSION['loginUser']['id'] != $id)
            return "inline";
        return "none";
    }

    static function statusDisplay($status) {
        if ($status == 'Not Accept')
            return "inline";
        return "none";
    }

    static function statusHide($status) {

        if ($_SESSION['loginUser'] && $status != 'Not Accept')
            return "inline";
        return "none";
    }

    static function mixDisplay($status) {
        if ($status == 'Accept' || $_SESSION['loginUser']['role'] == 'Admin')
            return "inline";
        return "none";
    }

}
