<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once  "Model.php";

/**
 * Description of UserModel
 *
 * @author mahmoud
 */
class UserModel extends Model {
 
    //put your code here
  //  function __autoload($class_name) {
       
   // }
    function __construct($conn, $tbName) {
        parent::__construct($conn, $tbName);
    }

    function addUser($data) {
        return $this->insert($data);
    }

    function deleteUser($id) {
        return $this->delete("id=$id");
    }

    function listUsers() {
        return $this->selectAll();
    }

    function getUserById($id) {
        return $this->select("id=$id");
    }
    
    function updateUserById($id,$data) {
            return $this->update($data, "id=$id");
    }
    
    function getUserByEmail($email) {
        return $this->select("email='"."$email"."'");
    }

}


