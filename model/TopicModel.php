<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class TopicModel extends Model {

    function __construct($conn, $tbName) {
        parent::__construct($conn, $tbName);
    }

    function addTopic($data) {
        //default configurations
        $data["status"] = "Accept";
        $data["stickiness"] = "Not Sticky";
        return $this->insert($data);
    }

    function deleteTopic($id) {
        return $this->delete("id=$id");
    }

    function getTopicsBySubForumId($subForumId) {
        return $this->select("forumId=$subForumId");
    }

    function getTopicsByUserId($userId) {
        return $this->select("userId=$userId");
    }

    function listTopics() {
        return $this->selectAll();
    }

    function getTopicById($id) {
        return $this->select("id=$id");
    }

    function updateTopicById($id, $data) {
        return $this->update($data, "id=$id");
    }

    function setTopicStatusAcceptReplies($id) {
        $data["status"] = "Accept";
        return $this->update($data, "id=$id");
    }

    function setTopicStatusNotAcceptReplies($id) {
        $data["status"] = "Not Accept";
        return $this->update($data, "id=$id");
    }

    function setTopicStickinessSticky($id) {
        $data["stickiness"] = "Sticky";
        return $this->update($data, "id=$id");
    }

    function setTopicStickinessNotSticky($id) {
        $data["stickiness"] = "Not Sticky";
        return $this->update($data, "id=$id");
    }

}

?>