<?php
include_once 'StandardForumModel.php';

class ForumModel extends StandardForumModel {

    function __construct($conn, $tbName, $ForumName) {
        parent::__construct($conn, $tbName);
        if (!$this->select("id=1"))
            $this->intiate($ForumName,NULL,"Working");
    }

    function setForumWorking() {
        return $this->changeStatus(1, "Working");
    }

    function setForumNotWorking() {
        return $this->changeStatus(1, "Not Working");
    }

    function changeForumName($ForumName) {
        return $this->changeName(1, $ForumName);
    }

    function getForumStatus() {
        return $this->getStatus(1);
    }

    function getForumName() {
        return $this->getName(1);
    }

}

?>